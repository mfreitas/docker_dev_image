echo "Starting up Notebook"
 
#EDIT THE FOLLOWING. CHANGE TO YOUR ORGS LICENSE_ID
SET "docker_image=mfreitas/docker_dev_image"
 
#EDIT THE FOLLOWING. CHANGE TO YOUR PREFERRED HTTP PORT
SET port=8888
 
#SET "share=/c/"

SET "share=/%cd%"
SET "share=%share:\=/%"
SET "share=%share:C:=c%"
SET "share=%share:D:=d%"

SET "docker_bin=docker.exe"

%docker_bin% login registry.gitlab.com
%docker_bin% pull registry.gitlab.com/%docker_image%
%docker_bin% run -it -p %port%:%port% -v "%share%":"/data" --name devserver registry.gitlab.com/%docker_image% /bin/bash
