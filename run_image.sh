#!/usr/bin/env bash
echo "Starting up Notebook"

#EDIT THE FOLLOWING. CHANGE TO YOUR ORGS LICENSE_ID
docker_image="mfreitas/docker_dev_image"
docker_registry="registry.gitlab.com"

#EDIT THE FOLLOWING. CHANGE TO YOUR PREFERRED HTTP PORT
port=8888

share=${PWD}
docker login $docker_registry
docker pull $docker_registry/$docker_image
docker run -d \
      -p $port:$port \
      -v "$share":"/data" \
      --name devserver \
      $docker_registry/$docker_image  /bin/bash -c $1

sleep 2

open http://localhost:$port
