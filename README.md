# DSCLUB DOCKER IMAGE
## RELEASE 0.1

## BUILD
`docker build -t mfreitas/dsclub_jupyter:0.1 .`

## RUN
```
docker run -it -p 8888:8888 -v $PWD:/data \
  --name notebo./run   okserver \
  mfreitas/dsclub_jupyter:0.1 /bin/bash -c "jupyter notebook \
  --ip 0.0.0.0 --no-browser --allow-root --NotebookApp.token='' \
  --notebook-dir='/data'"
```

## STOP
`docker container stop notebookserver`

## DELETE CONTAINER
`docker rm notebookserver`

## DELETE IMAGE 
`docker rmi dsclub_jupyter:0.1`

## CLEAN IMAGES AND CONTAINERS (DANGEROUS)
```
docker container prune -f
docker image prune -a
```
